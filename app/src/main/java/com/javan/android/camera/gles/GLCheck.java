package com.javan.android.camera.gles;

import android.opengl.GLES10;
import android.opengl.GLES11Ext;
import android.util.Log;

import com.javan.android.camera.BuildConfig;
import com.javan.android.camera.utils.StackTraceInfo;

/**
 * @since 2019/03/07
 * @author Javan
 * @version 1.0
 **/
class GLCheck {

    private static final String TAG = "GLCheck";

    static void glCheck(StackTraceInfo stackTraceInfo, String expression) {
        if (!BuildConfig.DEBUG) {
            return;
        }

        // Get the last error
        int errorCode = GLES10.glGetError();

        if (errorCode != GLES10.GL_NO_ERROR) {
            String error = "Unknown error";
            String description = "No description";

            // Decode the error code
            switch (errorCode) {
                case GLES10.GL_INVALID_ENUM: {
                    error = "GL_INVALID_ENUM";
                    description = "An unacceptable value has been specified for an enumerated argument.";
                    break;
                }

                case GLES10.GL_INVALID_VALUE: {
                    error = "GL_INVALID_VALUE";
                    description = "A numeric argument is out of range.";
                    break;
                }

                case GLES10.GL_INVALID_OPERATION: {
                    error = "GL_INVALID_OPERATION";
                    description = "The specified operation is not allowed in the current state.";
                    break;
                }

                case GLES10.GL_STACK_OVERFLOW: {
                    error = "GL_STACK_OVERFLOW";
                    description = "This command would cause a stack overflow.";
                    break;
                }

                case GLES10.GL_STACK_UNDERFLOW: {
                    error = "GL_STACK_UNDERFLOW";
                    description = "This command would cause a stack underflow.";
                    break;
                }

                case GLES10.GL_OUT_OF_MEMORY: {
                    error = "GL_OUT_OF_MEMORY";
                    description = "There is not enough memory left to execute the command.";
                    break;
                }

                case GLES11Ext.GL_INVALID_FRAMEBUFFER_OPERATION_OES: {
                    error = "GL_INVALID_FRAMEBUFFER_OPERATION";
                    description = "The object bound to FRAMEBUFFER_BINDING is not \"framebuffer complete\".";
                    break;
                }
            }

            // Log the error
            String errStringBuilder = "An internal OpenGL call failed in " +
                    stackTraceInfo.getFileName() + '(' + stackTraceInfo.getLineNumber() + ")." +
                    "\nExpression:\n   " + expression +
                    "\nError description:\n   " + error + "\n   " + description + "\n" +
                    '\n';
            Log.e(TAG, errStringBuilder);
        }
    }
}
