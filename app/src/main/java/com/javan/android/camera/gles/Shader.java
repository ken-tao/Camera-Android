package com.javan.android.camera.gles;

import android.content.res.Resources;
import android.opengl.GLES11;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.javan.android.camera.gles.glsl.Mat4;
import com.javan.android.camera.utils.StackTraceInfo;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Objects;

/**
 * Shader class (vertex, geometry and fragment)
 *
 * @since 2019/03/06
 * @author javan
 **/
public class Shader {

    private static final String TAG = "Shader";

    /**
     * Load shader from Android's Raw assets resource.
     *
     * @param resources Android's Resource object.
     * @param vertexShaderResId Resource Id of vertex shader.
     * @param fragmentShaderResId Resource Id of fragment shader.
     * @return True if loading succeeded, false if it failed
     * @throws IOException if an I/O error occurs.
     */
    public boolean loadFromResource(@NonNull Resources resources, int vertexShaderResId, int fragmentShaderResId) throws IOException {
        String vertexShaderCode, fragmentShaderCode;
        InputStream vertexShaderInputStream = resources.openRawResource(vertexShaderResId);
        vertexShaderCode = Shader.toString(vertexShaderInputStream);
        InputStream fragmentShaderInputStream = resources.openRawResource(fragmentShaderResId);
        fragmentShaderCode = Shader.toString(fragmentShaderInputStream);

        vertexShaderInputStream.close();
        fragmentShaderInputStream.close();

        if (vertexShaderCode != null && fragmentShaderCode != null) {
            return compile(vertexShaderCode, fragmentShaderCode);
        } else {
            return false;
        }
    }

    /**
     * Load both the vertex and fragment shaders from files
     *
     * @param vertexShaderFilename Path of the vertex shader file to load
     * @param fragmentShaderFilename Path of the fragment shader file to load
     * @return True if loading succeeded, false if it failed
     */
    public boolean loadFromFile(@NonNull String vertexShaderFilename,@NonNull String fragmentShaderFilename) {
        return false;
    }

    public void setUniform(String name, @NonNull final Mat4 mat4) {
        // TODO: Using RAII object to save and restore the program binding while uniforms are being set
        int currentProgram = mShaderProgram;
        int savedProgram = 0;
        try {
            if (currentProgram != 0) {
                // Enable program object
                int args[] = new int[1];
                GLES20.glGetIntegerv(GLES20.GL_CURRENT_PROGRAM, args, 0);
                GLCheck.glCheck(StackTraceInfo.getCurrentStackTraceInfo(), "GLES20.glGetIntegerv(GLES20.GL_CURRENT_PROGRAM, args, 0);");
                savedProgram = args[0];
                if (savedProgram != currentProgram) {
                    GLES20.glUseProgram(currentProgram);
                }

                // Store uniform location for further use outside constructor
                int location = getUniformLocation(name);
                if (location != -1) {
                    GLES20.glUniformMatrix4fv(location, 1, false, mat4.getArray(), 0);
                }
            }
        } finally {
            if (savedProgram != 0) {
                GLES20.glUseProgram(savedProgram);
            }
        }
    }

    /**
     * Get the underlying OpenGL handle of the shader.
     *
     * @return OpenGL handle of the shader or 0 if not yet loaded
     */
    public int getNativeHandle() {
        return mShaderProgram;
    }

    public static void bind(@Nullable final Shader shader) {
        if (shader != null && shader.mShaderProgram > 0) {
            // Enable the program
            GLES20.glUseProgram(shader.mShaderProgram);

            shader.bindTextures();


        } else {
            GLES20.glUseProgram(0);
        }
    }
    /**
     * If one of the arguments is NULL, the corresponding shader is not created.
     *
     * @param vertexShaderCode Source code of the vertex shader
     * @param fragmentShaderCode Source code of the fragment shader
     * @return True on success, false if any error happened
     */
    private boolean compile(@NonNull String vertexShaderCode, @NonNull String fragmentShaderCode) {
        // Destroy the shader if it was already created
        if (mShaderProgram > 0) {
            GLES20.glDeleteProgram(mShaderProgram);
            GLCheck.glCheck(StackTraceInfo.getCurrentStackTraceInfo(), "GLES20.glDeleteProgram(mShaderProgram);");
            mShaderProgram = 0;
        }

        int shaderProgram = GLES20.glCreateProgram();
        int vertexShader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
        GLES20.glShaderSource(vertexShader, vertexShaderCode);
        GLES20.glCompileShader(vertexShader);

        int[] compiled = new int[1];
        // check the compile log.
        GLES20.glGetShaderiv(vertexShader, GLES20.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 0) {
            Log.e(TAG, "Failed to compile vertex shader: ");
            Log.e(TAG, " " + GLES20.glGetShaderInfoLog(vertexShader));

            GLES20.glDeleteShader(vertexShader);
            GLES20.glDeleteProgram(shaderProgram);
            return false;
        }

        GLES20.glAttachShader(shaderProgram, vertexShader);
        GLES20.glDeleteShader(vertexShader);

        int fragmentShader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
        GLES20.glShaderSource(fragmentShader, fragmentShaderCode);
        GLES20.glCompileShader(fragmentShader);

        GLES20.glGetShaderiv(fragmentShader, GLES20.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 0) {
            Log.e(TAG, "Failed to compile fragment shader: ");
            Log.e(TAG, " " + GLES20.glGetShaderInfoLog(fragmentShader));

            GLES20.glDeleteShader(fragmentShader);
            GLES20.glDeleteProgram(shaderProgram);
            return false;
        }

        GLES20.glAttachShader(shaderProgram, fragmentShader);
        GLES20.glDeleteShader(fragmentShader);

        GLES20.glLinkProgram(shaderProgram);
        int[] linkStatus = new int[1];
        GLES20.glGetProgramiv(shaderProgram, GLES20.GL_LINK_STATUS, linkStatus, 0);
        if (linkStatus[0] != GLES20.GL_TRUE) {
            Log.e(TAG, "Could not link program: ");
            Log.e(TAG, GLES20.glGetProgramInfoLog(shaderProgram));
            GLES20.glDeleteProgram(shaderProgram);
            return false;
        }

        mShaderProgram = shaderProgram;

        return true;
    }

    /**
     * Bind all the textures used by the shader. Like mask textures or color map
     */
    private void bindTextures() {

        // TODO: Bind all shader textures.

        GLES20.glActiveTexture(GLES11.GL_TEXTURE0);
        GLCheck.glCheck(StackTraceInfo.getCurrentStackTraceInfo(), "GLES20.glActiveTexture(GLES11.GL_TEXTURE0);");
    }

    /**
     * Get the location ID of a shader uniform
     *
     * @param name Name of the uniform variable to search
     * @return Location ID of the uniform, or -1 if not found
     */
    private int getUniformLocation(@NonNull String name) {
        // Check the cache
        if (mUniforms.containsKey(name)) {
            // Already in cache, return it
            return Objects.requireNonNull(mUniforms.get(name));
        } else {
            // Not in cache, request the location from OpenGL
            int location = GLES20.glGetUniformLocation(mShaderProgram, name);
            mUniforms.put(name, location);

            if (location == -1) {
                Log.e(TAG, "Uniform \"" + name + "\" not found in shader");
            }

            return location;
        }
    }

    /**
     * This can move to a IOUtils.java file.
     *
     * @param inputStream byte data input stream for read.
     * @return always return a String, but it maybe empty.
     * @throws IOException if an I/O error occurs.π
     */
    private static String toString(@NonNull InputStream inputStream) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(inputStream);
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        int result = bis.read();
        while(result != -1) {
            buf.write((byte) result);
            result = bis.read();
        }
        // StandardCharsets.UTF_8.name() > JDK 7
        return buf.toString("UTF-8");
    }

    /**
     * OpenGL identifier for the program
     */
    private int mShaderProgram;
    /**
     * Shader program location cache.
     */
    private HashMap<String, Integer> mUniforms = new HashMap<>(3);
}
