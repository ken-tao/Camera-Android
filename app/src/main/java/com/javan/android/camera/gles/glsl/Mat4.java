package com.javan.android.camera.gles.glsl;

import android.opengl.Matrix;

/**
 * @since 2019/03/10
 **/
public class Mat4 {

    public Mat4() {
        // Init with identity matrix.
        Matrix.setIdentityM(array, 0);
    }

    public float[] getArray() {
        return array;
    }

    /**
     * Array to hold matrix data.
     */
    private float array [] = new float[4*4];
}
