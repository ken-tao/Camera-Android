package com.javan.android.camera.utils;

/**
 * This class help user to debug error happen.
 *
 * @since 2019/03/07
 * @author Javan
 * @version 1.0
 **/
public class StackTraceInfo {
    private String fileName;
    private String className;
    private String methodName;
    private int lineNumber;

    /**
     * Get current stack trace info.
     *
     * @return return an info object.
     */
    public static StackTraceInfo getCurrentStackTraceInfo() {
        StackTraceInfo info = new StackTraceInfo();
        int ORIGIN_STACK_INDEX = 2;
        StackTraceElement traceElement = Thread.currentThread().getStackTrace()[ORIGIN_STACK_INDEX];
        info.fileName = traceElement.getFileName();
        info.className = traceElement.getClassName();
        info.methodName = traceElement.getMethodName();
        info.lineNumber = traceElement.getLineNumber();
        return info;
    }

    public String getFileName() {
        return fileName;
    }

    public String getClassName() {
        return className;
    }

    public String getMethodName() {
        return methodName;
    }

    public int getLineNumber() {
        return lineNumber;
    }
}
